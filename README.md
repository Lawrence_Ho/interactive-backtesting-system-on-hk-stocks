# Interactive Backtesting System on HK Stocks #

This is a shiny webapp developed in R. User can input an expression to generate an alpha score for each stock in the dataset, the score will determine which bucket the stock will go into in a natural backtest. The testing results are shown in a performance matrix as well as various plots.

User can also manage the dataset by adding or removing symbols from dataset. The whole dataset can be updated to incorporate the latest data from Yahoo Finance.
Currently hosted on Shinyapps.io: [https://jason100100.shinyapps.io/APP1/](https://jason100100.shinyapps.io/APP1/)

### Version ###
* v1:
This project was started to fulfill the course requirement of FINA 4414, Spring 2015, HKUST. Author: Lawrence Ho

### How do I get set up? ###

This application is written in R-3.1.1
#### Library used: ####
* shiny
* quantmod
* backtest
* zoo

#### Start the server: ####
After you downloaded the zip of source or cloned the project, please set your working directory in R to:

* /app
* run command "runApp()"

or if you have R studio installed, just open the server.R or ui.R, and click run "Run App" button

### Available Data and Functions ###
Currently, only daily price-volume data are available, includes:

* open
* high
* low
* close
* volume
* adj.close : adjusted close price provided by Yahoo
* ret : return of previous period
* future.ret : return used for evaluation in backtesting, don't use it as an alpha input

Functions of standard R package and the libraries listed above can be used, some useful ones can be:

* rollmean : simple moving average provided by quantmod
* diff : lagged difference
* log : natural logarithms


### Sample Usage ###
A sample expression is provided in the server codes, which is a 10 days SMA reverting strategy on HSI constituent stocks. The result is quite satisfying.


### Contribution guidelines ###

For future developers:

* Introduce more types of data including fundamentals
* Introduce data of different period like weekly or monthly
* Introduce user account concept, so user can have their dataset and settings
* Introduce data range settings
* Improve security, currently the expression is parse and evaluated without any filtering, this poses serious security concerns
* Improve efficiency of data loading etc
* Writing tests
* Please contact the admin for write access and continue on this [repo](https://bitbucket.org/Lawrence_Ho/interactive-backtesting-system-on-hk-stocks) :)

For qaunt users:

* Use your creativity to generate more trading ideas!